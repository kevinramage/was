# Web Application Security

Web Application Security help you detect potential security issues in your favorite application

## Installation

### Docker

Execute run shell to install the application. This shell will clone git reposity of the two components required and create docker containers.

## Usage

### Create application

First step, create your application

![Applications view](/doc/images/applications.png)

### Start profiling

Open the application created and click on start profiling action

![Start profiling view](/doc/images/startProfiling.png)

### Configuration proxy browser

#### Chrome / IE

#### Firefox

### Navigate 

Now you can go back to your own application, navigate through the GUI, the system will analyze each requests.

### View warning identified

Click on warning menu

![Warning view](/doc/images/applicationWarnings.png)

