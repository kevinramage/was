REM Remove-Item -Recurse -Force wasf
REM Remove-Item -Recurse -Force wasp
rmdir wasf /s /q
rmdir wasp /s /q
git clone https://gitlab.com/kevinramage/wasp.git
git clone https://gitlab.com/kevinramage/wasf.git
docker-compose build
docker-compose up